var exports = module.exports = {};
var base = {"BRONZE":[1100,1025,950,875,800],"SILVER":[1450,1375,1300,1225,1150],"GOLD":[1800,1725,1650,1575,1500],"PLATINUM":[2150,2075,2000,1925,1850],"DIAMOND":[2500,2425,2350,2275,2200], "MASTER":[2575],"CHALLENGER":[2650]};
var divs = {'V' : 4, 'IV' : 3, 'III' : 2, 'II' : 1, 'I' : 0};
function normalize(value)
{
    return (value == undefined) ? "" : value.toLowerCase().split(" ").join("");
}

function calculateRate(tier, ent, history)
{
    var baseRate = base[tier][divs[ent.division]];
    var gw = ent.wins;
    var gl = ent.losses;
    
    var globalWinRatio = gw / (gw + gl);
    var gDelta = globalWinRatio - 0.5;
    var RECENT = history['matches'].length;
    var START = history['matches'].length - 10;
    console.log(START);
    var tot = 0;
    for (var i = START; i < RECENT; ++i)
    {
        tot += history['matches'][i]['participants'][0]['stats']['winner'] ? 1 : 0;
    }
    var recentWeight = RECENT / (gw + gl);
    var globalWeight = 1 - recentWeight;
    var recentWinRatio = tot / RECENT;
    console.log(history['matches']);
    var rDelta = recentWinRatio - 0.5;
    var delta = (recentWinRatio - globalWinRatio) / globalWinRatio;
    return  { 
                rate:   baseRate + gDelta * 100 * globalWeight + rDelta * 100 * recentWeight - (ent.isInactive ? 75 : 0) + (tier === "CHALLENGER" ? ent.leaguePoints / 3 : 0),
                global: Math.round(Number(globalWinRatio) * 100),
                recent: Math.round(Number(recentWinRatio) * 100)
            };
}
exports.base = base;
exports.normalize = normalize;
exports.calc = calculateRate;
