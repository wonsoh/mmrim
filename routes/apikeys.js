var keys = ['8bf7b960-bcb1-4d75-b8d2-0caf000dd6c3', '35f4affb-9858-4153-88b3-5c72c65db5e3', 'b9023b28-faf9-4812-b616-c87707ad18b6'];
var counter = 0;

var getKey = function()
{
    var key = keys[counter];
    counter = (counter + 1) % keys.length;
    return key;
}

module.exports.getKey = getKey;
