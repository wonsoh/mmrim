var express = require('express');
var router = express.Router();
var index = require('./index.js');
var xmpp = require('node-xmpp');
var EventEmitter = require('events').EventEmitter;
var qbox = require('qbox');
var parser = require('xml2js').parseString;
var extend = require("xtend");
var cors = require('cors');
var corsOptions = {
  origin: 'http://u.mmr.im'
};
var StatusDB = index.StatusDB;
var SERVERS = {
    nar: 'chat.na1.lol.riotgames.com',
    na: 'chat.na2.lol.riotgames.com',
    euw: 'chat.eu.lol.riotgames.com',
    eune: 'chat.eun1.lol.riotgames.com',
    kr: 'chat.kr.lol.riotgames.com',
    oce: 'chat.oc1.lol.riotgames.com',
    br: 'chat.br.lol.riotgames.com',
    ru: 'chat.ru.lol.riotgames.com',
    tr: 'chat.tr.lol.riotgames.com',
    lan: 'chat.la1.lol.riotgames.com',
    las: 'chat.la2.lol.riotgames.com'
};
var PRESENCE = {
    online: 'online',
    away: 'away',
    dnd: 'dnd'
};


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/status', cors(corsOptions), function(req, res, next) {
    var id = req.query.id;
    var region = req.query.region;
    StatusDB.find({sumId : id, region : region}, function(err, list) {
        console.log(list.length);
        if (list.length)
        {
            res.send(list[0].message);
        }
        else
        {
            res.status(404).send();
        }
    });
});

router.route('/verify').post(cors(corsOptions),function (req, res, next)
{
    var conn;
    var events = new EventEmitter();
    var $ = qbox.create();
    var internalEvents = new EventEmitter();
    var username = req.body.username;
    var password_prefix = 'AIR_';
    var password = req.body.password;
    var resource = 'xiff';
    var domain = 'pvp.net';
    var port = 5223;
    var id = req.body.id;
    var legacySSL = true;
    var region = req.body.region;
    var statusMsg = req.body.status;

    conn = new xmpp.Client({
            jid: username + '@' + domain + '/' + resource,
            password: password_prefix + password,
            host: SERVERS[region],
            port: port,
            legacySSL: legacySSL
        });
    conn.on('error', function (err) {
        res.status(400).send(err);
    });
    conn.on('online', function (data) {
        if(data['jid'].user == 'sum' + id)
        {
            StatusDB.update({sumId : "" + id, region : region},
                            {sumId : "" + id, region : region, message : statusMsg}, {upsert : true}, function(err, raw, list) {
                                if (!err)
                                {

                                }
                            });
            res.status(200).send(data);
            conn.end();
        }
        else
        {
            res.status(400).send({a : "FOO"});
        }
    });
    
});

module.exports = router;
