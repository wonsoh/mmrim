var express = require('express');
var router = express.Router();
var https = require('https');
var league = require('leagueapi');
var redis = require('redis');
var fs = require('fs');
//var client = redis.createClient(6379, 'rquery.ga', {});
//client.auth("JackPot45", function(err,reply){ console.log(reply) });
var sm = require('./summoner');
var _ = require('underscore');
var domain =    {
                    'NA1' : 'spectator.na.lol.riotgames.com:80',
                    'EUW1' : 'spectator.euw1.lol.riotgames.com:80',
                    'EUN1' : 'spectator.eu.lol.riotgames.com:8080',
                    'KR' : 'spectator.kr.lol.riotgames.com:80',
                    'OC1' : 'spectator.oc1.lol.riotgames.com:80',
                    'BR1' : 'spectator.br.lol.riotgames.com:80',
                    'LA1' : 'spectator.la1.lol.riotgames.com:80',
                    'LA2' : 'spectator.la2.lol.riotgames.com:80',
                    'RU' : 'spectator.ru.lol.riotgames.com:80',
                    'TR1' : 'spectator.tr.lol.riotgames.com:80'
                };
league.init('d1393d0c-bb62-448e-ae80-b57379493450', 'na');

router.get('*', function(req, res, next) {
    //genFile("X", 'x.x.x');
    //res.download("./foo.js", 'spectate.js', function(err){ console.log("X"); });
    var id = req.path.substring(1);
    res.sendFile("./specfiles/spec" + id + ".cmd", {root : __dirname, dotfiles: 'deny',headers: {
        'x-timestamp': Date.now(),
        'x-sent': true
    }}, function(err){ res.download("./specfiles/spec" + id + ".cmd", 'spectate.cmd' ) });
    //res.render('index', { title: 'RQ ', name: req.query.gameid });
});

//router.route('/').post(function(req,res,next){console.log(req.body);});
function genFile(filename, data)
{
    //var path = '\\"C:\\Riot Games\\League of Legends\\RADS\\solutions\\lol_game_client_sln\\releases\\' + version + '\\deploy\\League of Legends.exe\\"';
    
    
    //var maestro = '\\"8394\\"';
    //var launcher = '\\"LoLLauncher.exe\\"';
    //var blank = '\\"\\"';
    var constant = '"spectator';
    var platformId = data.platformId;
    console.log(platformId);
    var url = domain[platformId];
    var gameId = data.gameId;
    var key = data.observers.encryptionKey;
    var out = [constant, url, key, gameId, platformId + '"'];
    var cmd = out.join(" ");
    console.log(out.join(" "));
    /**var stream = fs.createWriteStream("./specfiles/" + filename);
        //stream.once('open', function(fd) {
    stream.write("var oShell = WScript.CreateObject(\"WScript.Shell\");");
    stream.write("oShell.run(\"" + cmd + "\");");
    stream.end();**/
    fs.readFile("./specfiles/template.bat", 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        var result = data.replace(/SPECTATOR_DATA/g, cmd);
        
        fs.writeFile("./specfiles/" + filename, result, 'utf8', function (err) {
            if (err) {
                return console.log(err);
            }
        });
    });
    //});
}

module.exports = router;
module.exports.genFile = genFile;
