var express = require('express');
var router = express.Router();
var league = require('leagueapi');
var redis = require('redis');
var utf8 = require('utf8');
//var geoip = require('geoip-lite');
//var client = redis.createClient(6379, 'rquery.ga', {});
//client.auth("JackPot45", function(err,reply){ console.log(reply) });
var sm = require('./summoner');
var spec = require('./spectate');
var _ = require('underscore');
var regions = {br:'br', eune:'eune', euw:'euw', kr:'kr', lan:'lan', las:'las', na:'na', oce:'oce', ru:'ru', tr:'tr'};
var locs = {en:'en_US', fr:'fr_FR', zh:'zh_CN', ko:'ko_KR', ru:'ru_RU'};
var fs = require('fs');
var apikey = require('./apikeys.js');
league.init(apikey.getKey(), 'na');
var mongoose = require('mongoose');
var port = ((process.env.PORT == 443) ? "https://" : "http://");
var ver = "5.4.1";

var Schema = mongoose.Schema;
mongoose.connect('mongodb://access:JackPot45@ds059821.mongolab.com:59821/fanse');

var SumSchema = new Schema({curr : String, prev : String, sumId : String, region : String, norm : String});
var SumDB = mongoose.model('SumDB', SumSchema);
var StatusSchema = new Schema({sumId : String, region : String, message : String});
var StatusDB = mongoose.model('StatusDB', StatusSchema);
/* GET home page. */
router.get('/',function(req, res, next) {
    if(req.headers['user-agent'].indexOf("MSIE") >= 0)
    {
        res.render('index_legacy', { title: 'MMR.im ', name: "", frontPage : true });
    }
    else
    {
        res.render('index', { title: 'MMR.im ', name: "", frontPage : true });
    }
});

router.get('/freeRotation', function(req,res,next) {
    var region = sm.normalize(req.query.region);
    if(regions[region]) {
        league.Static.getChampionList({champData: 'all', dataById : true, locale : locs[req.i18n.lng().substring(0,2)]}, region, function(err, raw) {
            fs.writeFile("static/" + region + "/champs_" + req.i18n.lng().substring(0,2) + ".json", JSON.stringify(raw), function(err) { console.log(err); });
        });
        
        league.init(apikey.getKey(), region);
        var champs = JSON.parse(fs.readFileSync("static/" + region + "/champs_" + req.i18n.lng().substring(0,2) + ".json"))['data'];
        league.getChampions(true, region, function(err, raw) {
            var championList = _.map(raw, function(p) { return champs[p.id] });
            res.status(200).send({cList : championList});
        });
    }
});

router.get('/fetchPage', function(req, res, next) {
    var isIE = (req.headers['user-agent'].indexOf("MSIE") >= 0);
    if (isIE)
    {
        res.render('fetched_legacy', { region: req.query.region, sumName: req.query.sumName});
    } else
    {
        res.render('fetched', { region: req.query.region, sumName: req.query.sumName});
    }
});

router.get('/fetch', function(req, res, next) {
    var isIE = (req.headers['user-agent'].indexOf("MSIE") >= 0);
    var glob = res;
    var norm = sm.normalize(req.query.sumName);
    var region = sm.normalize(req.query.region);
    if(regions[region]) {
        league.Static.getChampionList({champData: 'all', dataById : true, locale : locs[req.i18n.lng().substring(0,2)]}, region, function(err, raw) {
            fs.writeFile("static/" + region + "/champs_" + req.i18n.lng().substring(0,2) + ".json", JSON.stringify(raw), function(err) { console.log(err); });
        });
        league.Static.getSummonerSpellList({ spellData: 'all', dataById: true, locale : locs[req.i18n.lng().substring(0,2)]}, region, function(err, raw) {
            fs.writeFile("static/" + region + "/spells_" + req.i18n.lng().substring(0,2) + ".json", JSON.stringify(raw), function(err) { console.log(err); });
        });
        league.Static.getItemList({ itemListData: 'all', locale : locs[req.i18n.lng().substring(0,2)] }, region, function(err, raw) {
            fs.writeFile("static/" + region + "/items_" + req.i18n.lng().substring(0,2) + ".json", JSON.stringify(raw), function(err) { console.log(err); });
        });
        SumDB.find({norm : norm, region : region}, function(err, list) {
            if(list.length) {
                var summonerId = list[0].sumId;
                league.Summoner.getByID(summonerId, region, function(err, raw) {
                    if (!err) {
                        var summoner = raw[summonerId];
                        if(isIE)
                        {
                            res.render('fetch_legacy', raw[summonerId]);
                        }
                        else
                        {
                            res.render('fetch', raw[summonerId]);
                        }
                        if (list[0].curr != summoner.name) {
                            SumDB.update({sumId : "" + summoner.id, region : region},
                                            {
                                                curr : summoner.name,
                                                sumId : "" + summoner.id,
                                                region : region,
                                                prev : list[0].curr,
                                                norm : sm.normalize(summoner.name)
                                            }, 
                                            {upsert : true}, 
                                            function(err, raw, list) { if (!err) { } });
                        }
                        else { /* NO CHANGE */ }
                    } else {
                        if(isIE)
                        {
                            res.render('fetch_legacy', { unfound:true } );
                        }
                        else
                        {
                            res.render('fetch', { unfound:true } );
                        }
                        SumDB.remove({sumId : summonerId, region : region}, function(err,removed) {
                            if (!err) { }
                        });
                    }
                });
            }
            else {
                console.log("DB NONEXISTENT");
                league.init(apikey.getKey(), region);
                league.Summoner.getByName(norm, region, function(err, raw) {
                    if(!err) { //summoner existent
                        var summoner = raw[norm];
                        if(isIE)
                        {
                            res.render('fetch_legacy', summoner);
                        }
                        else
                        {
                            res.render('fetch', summoner);
                        }
                        SumDB.update({
                                        curr : summoner.name,
                                        sumId : "" + summoner.id,
                                        region : region,
                                        norm : norm, prev : ""},
                                        {
                                            curr : summoner.name,
                                            sumId : "" + summoner.id,
                                            region : region,
                                            norm : norm
                                        },
                                        {upsert : true}, function(err, raw, list) { if (!err) { } });
                    } else {
                        if(isIE)
                        {
                            res.render('fetch_legacy', { unfound:true } );
                        }
                        else
                        {
                            res.render('fetch', { unfound:true } );
                        }
                    }
                });
            }
        });
    } else {
        if(isIE)
        {
            res.render('fetch_legacy', { unfound:true } );
        }
        else
        {
            res.render('fetch', { unfound:true } );
        }
    }
});

router.get('/fetchLeagueData', function(req, res, next) {
    var id = req.query.id;
    var region = sm.normalize(req.query.region);
    league.getLeagueEntryData(id, region, function(err, raw){
        if(!err) {
            res.send(raw[id]);
        } else {
            res.status(404).send({a : "FOO"});
        }
    });
});

router.post('/rate', function(req, res, next) {
    console.log(req.body);
    var entry = req.body;
    var tier = entry.tier;
    var id = entry.id;
    var region = entry.region;
    league.getMatchHistory(id, {rankedQueues: ["RANKED_SOLO_5x5"]}, region, function (err, hist){
        if(!err)
        {
            var obj = sm.calc(tier, entry, hist);
            obj.rate = Math.round(obj.rate);
            res.status(200).send(obj);
        } else {
            res.status(500).send({a: "BAR"});
        }
    });
});
router.route('/history').post(function (req, res, next)
{
    var id = req.body.id;
    var region = req.body.region;
    var beg = req.body.beginIndex;
    var queues = req.body.queues;
    console.log(req.body);
    league.init(apikey.getKey(), region);
    if(queues == "SOLO") {
         queues = ["RANKED_SOLO_5x5"];
    } else {
        queues = ["RANKED_TEAM_5x5","RANKED_TEAM_3x3"];
    }
    league.getMatchHistory(id, { beginIndex : beg, rankedQueues: queues }, region, function(err, raw)
    {
        if(!err && raw.matches)
        {
            console.log("FOO");
            var champs = JSON.parse(fs.readFileSync("static/" + region + "/champs_" + req.i18n.lng().substring(0,2) + ".json"))['data'];
            var spells = JSON.parse(fs.readFileSync("static/" + region + "/spells_" + req.i18n.lng().substring(0,2) + ".json"))['data'];
            var items = JSON.parse(fs.readFileSync("static/" + region + "/items_" + req.i18n.lng().substring(0,2) + ".json"))['data'];
            //console.log(raw);
            
            var data = raw.matches.reverse();
            var championList = _.map(data, function(p) { return champs[p.participants[0].championId]; });
            var spellList = _.map(data, function(p) { return { d : spells[p.participants[0].spell1Id], f : spells[p.participants[0].spell2Id] }  });
            var itemList = _.map(data, function(p) { return _.map([ p.participants[0].stats.item0,
                                                                    p.participants[0].stats.item1,
                                                                    p.participants[0].stats.item2,
                                                                    p.participants[0].stats.item3,
                                                                    p.participants[0].stats.item4,
                                                                    p.participants[0].stats.item5,
                                                                    p.participants[0].stats.item6 ], function(i) { return items[i]; }); 
                                                    });
            res.status(200).send({arr : data, cList : championList, sList : spellList, iList : itemList });
        }
        else
        {
            res.status(404).send({a: "FOO"});
        }
    });
});

router.route('/recent').post(function (req, res, next)
{
    var id = req.body.id;
    var region = req.body.region;
    league.init(apikey.getKey(), region);
    league.getRecentGames(id, region, function(err, raw)
    {
        if(!err)
        {
            var champs = JSON.parse(fs.readFileSync("static/" + region + "/champs_" + req.i18n.lng().substring(0,2) + ".json"))['data'];
            var spells = JSON.parse(fs.readFileSync("static/" + region + "/spells_" + req.i18n.lng().substring(0,2) + ".json"))['data'];
            var items = JSON.parse(fs.readFileSync("static/" + region + "/items_" + req.i18n.lng().substring(0,2) + ".json"))['data'];

            //console.log(raw);
            //console.log(raw[0].fellowPlayers);
            var championList = _.map(raw, function(p) { return champs[p.championId]; });
            var spellList = _.map(raw, function(p) { return { d : spells[p.spell1], f : spells[p.spell2] }  });
            var itemList = _.map(raw, function(p) { return _.map([ p.stats.item0,
                p.stats.item1,
                p.stats.item2,
                p.stats.item3,
                p.stats.item4,
                p.stats.item5,
                p.stats.item6 ], function(i) { return items[i]; });
            });
            var fellowMap = _.object(_.map(raw, function(p) { return p.gameId }), _.map(raw, function(p) { return p.fellowPlayers }));
            //console.log(fellowMap);
            res.status(200).send({ arr : raw, cList : championList, sList : spellList, iList : itemList, fMap : fellowMap });
            
        }
        else
        {
            //console.log(err);
            res.status(404).send({a: "FOO"});
        }
    });
});

router.route('/ids').post(function (req, res, next)
{
    var id = req.body.id;
    var region = req.body.region;
    //console.log(id);
    if (Array.isArray(id))
    {
        id = id.join();
        
    }
    league.init(apikey.getKey(), region);
    league.Summoner.getByID(id, region, function(err, raw) {
        if(!err)
        {
            //console.log(raw);
            res.status(200).send(raw);
        }
        else
        {
            //console.log(err);
            res.status(404).send({a: "FOO"});
        }
    });
});

router.route('/timeline').post(function (req, res, next)
{
    var id = req.body.matchId;
    var region = req.body.region;
    league.getTimeline(id, region, function(err, raw) {
        if(!err)
        {
            res.status(200).send(raw);
        }
        else
        {
            res.status(404).send({a: "FOO"});
        }
    });
});

router.route('/game').post(function (req, res, next)
{
    var id = req.body.matchId;
    var region = req.body.region;
    league.getGame(id, region, function(err, raw)
    {
        if(!err)
        {
            var champs = JSON.parse(fs.readFileSync("static/" + region + "/champs_" + req.i18n.lng().substring(0,2) + ".json"))['data'];
            var spells = JSON.parse(fs.readFileSync("static/" + region + "/spells_" + req.i18n.lng().substring(0,2) + ".json"))['data'];
            var items = JSON.parse(fs.readFileSync("static/" + region + "/items_" + req.i18n.lng().substring(0,2) + ".json"))['data'];
            
            var championList = _.map(raw.participants, function(p) { return champs[p.championId]; });
            var spellList = _.map(raw.participants, function(p) { return { d : spells[p.spell1Id], f : spells[p.spell2Id] }  });
            var itemList = _.map(raw.participants, function(p) { return _.map([ p.stats.item0,
                                                                                p.stats.item1,
                                                                                p.stats.item2,
                                                                                p.stats.item3,
                                                                                p.stats.item4,
                                                                                p.stats.item5,
                                                                                p.stats.item6 ], function(i) { return items[i]; }); 
                                                    });
                                            
            res.status(200).send({ arr : raw, cList : championList, sList : spellList, iList : itemList });
        }
        else
        {
            //console.log(err);
            res.status(404).send({a: "FOO"});
        }
    });
});

router.get('/hall', function(req,res,next){
    res.render('halloffame');
});

router.route('/hallOfFame').get(function(req, res, next) { 
    var region = req.query.region;
    var tier = req.query.tier;
    res.set('charset','utf-8');
    console.log(region);
    console.log(tier);
    league.init(apikey.getKey(), region);
    league.getHallOfFame(region, tier, function(err,raw) {
        console.log(err);
        if(!err) {
            res.render('halllist', {lst: _.sortBy(raw["entries"], function(d) { return d["leaguePoints"]}), region: region});
        }
    });
});

router.route('/spec').get(function(req, res, next) { 
    //console.log(req.query.id);
    //console.log(req.query.region);
    var region = sm.normalize(req.query.region);
    if(req.query.id) {
        league.init(apikey.getKey(), region);
        league.getCurrentGame(req.query.id, region, function(err, raw){
            if(!err) {
                //console.log(raw);
                var parts = raw.participants;
                //console.log(region);
                var champs = JSON.parse(fs.readFileSync("static/" + region + "/champs_" + req.i18n.lng().substring(0,2) + ".json"))['data'];
                var spells = JSON.parse(fs.readFileSync("static/" + region + "/spells_" + req.i18n.lng().substring(0,2) + ".json"))['data'];
                var championList = _.map(parts, function(p) { return champs[p.championId] });
                var spellList = _.map(parts, function(p) { return { d : spells[p.spell1Id], f : spells[p.spell2Id] }  });
                //console.log(championList);
                //console.log(spellList);
                raw.cList = championList;
                raw.sList = spellList;
                spec.genFile("spec" + raw.gameId + ".cmd", raw);
                res.status(200).send(raw);
            }
            else {
                //console.log(err);
                res.status(404).send({a : "FOO"});
            }
        });
    }
});


router.get('/dat', function(req, res)
{
    SumDB.find({region : req.query.region}).lean().exec(function(err, list)
    {
        res.send(list);
    });
});

module.exports = router;
module.exports.SumDB = SumDB;
module.exports.StatusDB = StatusDB;
