var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var subdomain = require('subdomain');
var routes = require('./routes/index');
//var freegeoip = require('node-freegeoip');
//var routes_legacy = require('./routes/index_legacy');
var users = require('./routes/users');
var spectate = require('./routes/spectate');
var i18n = require('i18next');
var option = { resGetPath: 'public/locales/__lng__/__ns__.json' };
i18n.init(option);
var app = express();
//var lite = express();
process.on('uncaughtException', function(err){ console.log (err); });
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
i18n.registerAppHelper(app);
//i18n.registerAppHelper(lite);
var http = express();
http.get("*", function(req,res,next)
{
    if (process.env.PORT == '443')
    {
        res.redirect('https://fanse.me' + req.originalUrl);
    }
    /*var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    freegeoip.getLocation(ip, function(err, location) {
        if(!err)
        {
            var ctry = location.country_code;
            if(ctry == 'KR' || ctry == 'JP' || ctry == 'CN')// || ctry == 'US')
            {
                res.redirect('http://k.fanse.me' + req.originalUrl);
            }
            else
            {
                if (process.env.PORT == '443')
                {
                    res.redirect('https://fanse.me' + req.originalUrl);
                }
                else
                {
                    next();
                }
            }
        }
        else
        {
            next();
        }
    });*/
});
if (process.env.PORT == '443')
{
    http.listen(80);
}
/**
if (process.env.PORT == '443')
{
    app.get("*", function(req,res,next)
    {
        if(req.originalUrl.indexOf("users/status") >= 0)
        {
            next();
        }
        else
        {
            var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            freegeoip.getLocation(ip, function(err, location) {
                var ctry = location.country_code;
                if(!err)
                {
                    if(ctry == 'KR' || ctry == 'JP' || ctry == 'CN')
                    {
                        res.redirect('http://k.fanse.me' + req.originalUrl);
                    }
                    else
                    {
                        next();
                    }
                }
                else
                {
                    next();
                }
            });
        }
    });
}**/
// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
var sub = subdomain({ base : 'localhost', removeWWW : true });
app.use(sub);
app.use(i18n.handle);
app.use('/', routes);
//app.use('/legacy', routes_legacy);
//app.use('/lite', routes_legacy);

//app.use('/lite/', routes_legacy);
//if (process.env.PORT == 443)
//{
    app.use('/users', users);
//}
app.use('/spectate', spectate);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
