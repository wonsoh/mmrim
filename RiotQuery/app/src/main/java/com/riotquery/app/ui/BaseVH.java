package com.riotquery.app.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Nathan Walker on 3/22/15.
 */
public abstract class BaseVH<T> extends RecyclerView.ViewHolder {
    public BaseVH(View v) {
        super(v);
    }

    public abstract void bind(T data);
}
