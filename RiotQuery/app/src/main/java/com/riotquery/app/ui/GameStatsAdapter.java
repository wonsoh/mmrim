package com.riotquery.app.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.riotquery.app.R;
import com.riotquery.app.api.model.GameStats;

/**
 * Created by Jon on 3/23/15.
 */
public class GameStatsAdapter extends RVBaseAdapter<GameStats> {

    public GameStatsAdapter(OnItemClickListener<GameStats> listener) {
        super(listener);
    }

    public GameStatsView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.li_game_stats, parent, false);
        return new GameStatsView(view);
    }

}
