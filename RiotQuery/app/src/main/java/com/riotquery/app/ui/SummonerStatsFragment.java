package com.riotquery.app.ui;

import android.app.Fragment;
import android.os.Bundle;

/**
 * Created by Jon on 3/22/15.
 */
public class SummonerStatsFragment extends Fragment {
    //name, kda, most played, ...


    public static SummonerStatsFragment newInstance(String apiKey) {
        SummonerStatsFragment f = new SummonerStatsFragment();
        Bundle args = new Bundle();
        args.putString("API_KEY", apiKey);
        f.setArguments(args);

        return f;
    }

}
