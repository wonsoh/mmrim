package com.riotquery.app;

import android.app.Activity;
import android.app.Application;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.riotquery.app.ui.SummonerHistoryFragment;
import com.riotquery.app.ui.SummonerStatsFragment;

/**
 * Created by Jon on 3/15/15.
 */
public class WelcomeActivity extends Activity {
    public static final String TAG = WelcomeActivity.class.getSimpleName();

    private Application mApp;

    private EditText search;
    private Button go;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        mApp = (Application) getApplication();

        search = (EditText) findViewById(R.id.search_box);
        go = (Button) findViewById(R.id.search_button);

        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchText = search.getText().toString();
                String summonerName = TextUtils.equals(searchText, "") ? "" : searchText;
                //TODO - get apiKey from summonerName
                String apiKey = "";

                //Delegate the API call to respective inner Fragment's "load"
                FragmentTransaction t = getFragmentManager().beginTransaction();
                t.replace(R.id.summoner_stats_container, SummonerStatsFragment.newInstance(apiKey));
                t.replace(R.id.games_history_container, SummonerHistoryFragment.newInstance(apiKey));
                t.commit();
            }
        });
    }


    //@Override
    //protected int getSelfNavDrawerItem() {
     //   return NAVDRAWER_ITEM_SEPARATOR;
    //}


}
