package com.riotquery.app.ui;

/**
 * Created by Jon on 3/23/15.
 */
public interface OnItemClickListener<T> {
    public void onClick(T item);
}
