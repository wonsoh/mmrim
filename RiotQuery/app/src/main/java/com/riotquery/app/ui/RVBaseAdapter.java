package com.riotquery.app.ui;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jon on 3/9/15.
 */

public abstract class RVBaseAdapter<T> extends RecyclerView.Adapter<BaseVH<T>> {

    private static final String TAG = RVBaseAdapter.class.getSimpleName();

    public int layoutRID;

    //Internal member
    public List<T> data = new ArrayList<T>();
    OnItemClickListener<T> listener;

    public RVBaseAdapter(OnItemClickListener<T> listener) {
        this.listener = listener;
    }

    // TODO set onclicklistener


    @Override
    public void onBindViewHolder(final BaseVH<T> vh, final int position) {
        final T item = getItem(position);
        vh.bind(item);
        vh.itemView.setOnClickListener(listener == null ? null : new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size(); //or getCount()
    }


    public T getItem(int position) {
        return data.get(position);
    }

    public void setItems(List<T> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void clearItems() {
        data.clear();
        notifyDataSetChanged();
    }

    public int getCount() {
        Log.v(TAG, "Count: " + data.size());
        return data.size();
    }



}
