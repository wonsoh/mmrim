package com.riotquery.app.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.riotquery.app.R;
import com.riotquery.app.api.model.GameStats;

/**
 * Created by Jon on 3/23/15.
 */
public class SummonerHistoryFragment extends Fragment {

    public static final String TAG = SummonerHistoryFragment.class.getSimpleName();

    private RecyclerView rView;
    private String summonerName;


    public static SummonerHistoryFragment newInstance(String apiKey) {
        SummonerHistoryFragment f = new SummonerHistoryFragment();
        Bundle args = new Bundle();
        args.putString("API_KEY", apiKey);
        f.setArguments(args);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_summoner_history, container, false);

        rView = (RecyclerView) v.findViewById(R.id.games_list);

        rView.setLayoutManager(new LinearLayoutManager(getActivity()));
        rView.setItemAnimator(new DefaultItemAnimator());
        //rView.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), true, true));

        rView.setAdapter(new GameStatsAdapter(new OnItemClickListener<GameStats>() {
            @Override
            public void onClick(GameStats game) {
                //TODO
            }
        }));

        /*TODO - work "swipeRefreshable" into builder
        srLayout = (GeneralSwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        srLayout.setOnChildScrollUpListener(new GeneralSwipeRefreshLayout.OnChildScrollUpListener() {
            @Override
            public boolean canChildScrollUp() {
                return rView.canScrollVertically(-1);
            }
        });
        srLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });*/

        return v;

    }
}
