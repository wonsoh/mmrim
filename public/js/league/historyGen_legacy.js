//$("#regions").val($.QueryString("region"));
var off = $('#title1').offset().top - 50;
var region = $.QueryString("region") || $('#regions').val() ;
var popoverTemp = '<div style="width:200px" class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>';
var popoverTempH = '<div style="width:280px" class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>';
/* RANKED SUMMARY SECTION */
var rankSoloStats = {};
var origin =  window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
var summary = function() {
    $.get(origin + "/fetchLeagueData", {id : summonerId, region : region})
        .success(
        function(data) {
                for(var i = 0; i < data.length; ++i)
                {
                    var entry = data[i]
                    var entryId = "#" + entry["queue"];
                    var division = entry["tier"] + " " + entry["entries"][0]["division"];
                    $(entryId + " #division h6").html(division);
                    $(entryId + " #division img").prop("src", "/img/medals/" + division + ".png")
                    $(entryId + " #lp")
                        .html($("<h5>").html("LP: " + entry["entries"][0]["leaguePoints"]).css("margin","auto"));
                    if(entry["queue"] == "RANKED_SOLO_5x5") {
                        rankSoloStats = entry["entries"][0];
                        rankSoloStats.tier = entry.tier;
                        rankSoloStats.id = summonerId;
                        rankSoloStats.region = region;
                        calcMMR(rankSoloStats);
                    }
                    var miniSeries = entry["entries"][0]["miniSeries"];
                    if(miniSeries) {
                        var prog = miniSeries["progress"];
                        for(var i = 0; i < prog.length; ++i)
                        {
                            var p = prog[i];
                            if(p == "W")
                            {
                                $(entryId + " #miniSeries").append($("<i>").addClass("fa fa-check").css("color","green"));
                            } else if (p == "L")
                            {
                                $(entryId + " #miniSeries").append($("<i>").addClass("fa fa-times").css("color","red"));
                            } else if (p == "N")
                            {
                                $(entryId + " #miniSeries").append($("<i>").addClass("fa fa-minus").css("color","grey"));
                            }
                        }
                        $(entryId + " #miniSeries i").css("margin","0px 10px");
                        $(entryId + " #miniSeries").removeClass("hidden");
                    }
                }
        }
    ).fail(
        function(e) {

        }
    ).done(
        function(d) {

        }
    )
}

var calcMMR = function(ranked) {
    $.post(origin + "rate", ranked)
        .success(
        function(data) {
            $("#fanseRate").html($("<h5>").html(data.rate).css("margin", "auto"));
        }
    ).fail(
        function(e) {
            $("#fanseRate").html(" -- ");
        }
    ).done(
    )
}
/* SPECTATION SECTION */
function startSpin()
{
    $("#spin").fadeIn('fast');
}
function stopSpin()
{
    $("#spin").fadeOut('fast');
}
function craft(url, data)
{
    return "url('" + url + data + "')";
}
function formatTime(ss)
{
    return {mm : "" + Math.floor(ss / 60), ss : (ss % 60 < 10 ? "0" + ss % 60 : "" + ss % 60) };
}
var ver = '5.10.1';
var itemUrl = "https://ddragon.leagueoflegends.com/cdn/" + ver + "/img/item/";
var championUrl = "https://ddragon.leagueoflegends.com/cdn/" + ver + "/img/champion/";
var spellUrl = "https://ddragon.leagueoflegends.com/cdn/" + ver + "/img/spell/";
var iconUrl = "https://ddragon.leagueoflegends.com/cdn/" + ver + "/img/profileicon/";
//$('#info').modal('show');

$("#spec").click(function() {
    NProgress.start();startSpin();
    if(!$("#vs").html())
    {
        $.get(origin + "/spec", {id : summonerId, region : region}).success(function(data) {
            $.notify(i18n.t('notif.gameFound') + " -- " + i18n.t('notif.generating', {gameId : data.gameId}), "info");
            $(".currentheader").append("<th style = 'text-align : center'> Summoner </th>")
                .append("<th style = 'text-align : center'> Spell 1 </th>")
                .append("<th style = 'text-align : center'> Spell 2 </th>")
                .append("<th style = 'text-align : center'> Champion </th>");
            $('#currentGame100').append("<tr></tr>");
            $('#currentGame200').append("<tr></tr>");
            $('#vs').html("VS.");
            for (var i = 0; i < data.participants.length; ++i)
            {
                var part = data.participants[i];
                var champ = data.cList[i];
                var spell = data.sList[i];
                var di = spell.d.image;
                var ci = champ.image;
                var fi = spell.f.image;
                var dImg = $("<img>")
                                .prop("src", spellUrl + di.full)
                                .css("width", "32px").css("height", "32px");
                var fImg = $("<img>")
                                .prop("src", spellUrl + fi.full)
                                .css("width", "32px").css("height", "32px");
                var cImg = $("<img>")
                                .prop("src", championUrl + ci.full)
                                .css("width", "64px").css("height", "64px");
                dImg = di ? $("<span>").append(dImg).popover({trigger:'hover', html : true, title : spell.d.name, content: spell.d.description, template: popoverTempH, placement:'bottom'}) : dImg;
                fImg = fi ? $("<span>").append(fImg).popover({trigger:'hover', html : true, title : spell.f.name, content: spell.f.description, template: popoverTempH, placement:'bottom'}) : fImg;
                cImg = $("<span>").append(cImg).popover({trigger:'hover', html : true, title : $("<var>").html(champ.title), template: popoverTempH, placement:'bottom'});
                if (part.teamId == 100)
                {
                    $('#currentGame100 tr:last')
                        .append($(document.createElement("td")).html(part.summonerName))
                        .append($(document.createElement("td")).append(dImg).append("<br>").append($(document.createElement("span")).css("font-size", "small").html(spell.d.name)))
                        .append($(document.createElement("td")).append(fImg).append("<br>").append($(document.createElement("span")).css("font-size", "small").html(spell.f.name)))
                        .append($(document.createElement("td")).append(cImg).append("<br>").append($(document.createElement("span")).css("font-size", "small").html(champ.name)));
                    if (part.summonerName == summonerName){ $('#currentGame100 tr:last').addClass("active"); $('#currentGame100').addClass("success"); $('#currentGame200').addClass("danger"); }
                    $('#currentGame100').append("<tr></tr>");
                }
                else if (part.teamId == 200)
                {
                    $('#currentGame200 tr:last')
                        .append($(document.createElement("td")).html(part.summonerName))
                        .append($(document.createElement("td")).append(dImg).append("<br>").append($(document.createElement("span")).css("font-size", "small").html(spell.d.name)))
                        .append($(document.createElement("td")).append(fImg).append("<br>").append($(document.createElement("span")).css("font-size", "small").html(spell.f.name)))
                        .append($(document.createElement("td")).append(cImg).append("<br>").append($(document.createElement("span")).css("font-size", "small").html(champ.name)));
                    if (part.summonerName == summonerName){ $('#currentGame200 tr:last').addClass("active"); $('#currentGame200').addClass("success"); $('#currentGame100').addClass("danger"); }
                    $('#currentGame200').append("<tr></tr>");
                }
            }
            $("#downloadSpec").prop("href", "/spectate/" + data.gameId);
        }).fail(function(data) {
            $.notify(i18n.t('notif.unavailable') + " -- " + i18n.t('notif.notInCurrentGame'), "error");
            $("#specBody").addClass("hidden");
            //console.log(data.responseJSON);
            NProgress.done();stopSpin();
        }).done(function(data) {
            $("#specBody").removeClass("hidden");
            $('html, body').animate({ scrollTop: $('#spec').offset().top}, 'slow');
            $.notify(i18n.t('notif.done') + " -- " + i18n.t('notif.finished'), "success");
            NProgress.done();stopSpin();
        });
    }
    NProgress.done();stopSpin();
});
/* HISTORY GENERATING SECTION */
var beginIndex = 0;
var teamIndex = 0;
var getHist = function () {
    $.notify(i18n.t('notif.loadingRanked') + " -- " + i18n.t('notif.pleaseWait'), "info");
    NProgress.start();startSpin();
    $.post(origin + '/history', {id: summonerId, beginIndex: beginIndex, region: region, queues : "SOLO"})
        .success(function (data) {
            var arr = data.arr;
            for (var i = 0; i < arr.length; ++i) {
                var prot = $(".panelProto").clone().removeClass("panelProto");
                var stats = arr[i].participants[0].stats;
                var wl = stats.winner;
                var champ = data.cList[i];
                var spell = data.sList[i];
                var ci = champ.image;
                var di = spell.d.image;
                var fi = spell.f.image;
                var itemCol = data.iList[i];
                var summary = prot.find(".summary");
                var dImg = summary.find(".dImg");
                var fImg = summary.find(".fImg");
                var cImg = summary.find(".cImg");
                dImg.prop("src", spellUrl + di.full);
                fImg.prop("src", spellUrl + fi.full);
                cImg.prop("src", championUrl + ci.full);
                dImg = di ? dImg.popover({trigger:'hover', html : true, title : spell.d.name, content: spell.d.description, template: popoverTempH, placement:'bottom'}) : dImg;
                fImg = fi ? fImg.popover({trigger:'hover', html : true, title : spell.f.name, content: spell.f.description, template: popoverTempH, placement:'bottom'}) : fImg;
                cImg = ci ? cImg.popover({trigger:'hover', html : true, title : champ.name, content: champ.title, template: popoverTempH, placement:'bottom'}) : cImg;                
                prot.find(".panel-collapse").prop("id", "m" + arr[i].matchId);
                summary.find(".expand").prop("href", "#m" + arr[i].matchId).attr("data-parent", "#historyGroup").attr("match", arr[i].matchId);
                summary.find(".timeline").attr("match", arr[i].matchId);
                summary.find(".kills").html(stats.kills + "K");
                summary.find(".deaths").html(stats.deaths + "D");
                summary.find(".queueType").html(i18n.t("gameType." + arr[i].queueType));
                summary.find(".assts").html(stats.assists + "A");
                var ratio = stats.deaths ? Math.round(100 * (stats.kills + stats.assists) / stats.deaths) / 100 : "<code>PERFECT</code>";
                summary.find(".kda").html(ratio);
                summary.find(".lvl").html(i18n.t("global.level") + " " + stats.champLevel);
                for (var m = 0; m < itemCol.length; ++m)
                {
                    var ii = itemCol[m] ? summary.find(".i" + (m + 1)).prop("src", itemUrl + itemCol[m].image.full) : "";
                    ii = (ii == "") ? ii : ii.popover({trigger:'hover',
                                                        html : true, title : itemCol[m].name,
                                                        content: itemCol[m].description, 
                                                        template: popoverTempH, 
                                                        placement: 'bottom'});
                }
                summary.find(".ward").html(stats.sightWardsBoughtInGame);
                summary.find(".vision").html(stats.visionWardsBoughtInGame);
                summary.find(".minions").html(stats.minionsKilled);
                summary.find(".neutral").html(stats.neutralMinionsKilled);
                summary.find(".totalMinions").html((stats.minionsKilled + stats.neutralMinionsKilled));
                summary.find(".golds").html(stats.goldEarned);
                prot.removeClass("hidden");
                prot = wl ? prot.addClass("panel-success") : prot.addClass("panel-danger");
                $("#historyGroup").append(prot);
                var startTime = new Date(arr[i].matchCreation);
                var duration = formatTime(arr[i].matchDuration);
                prot.find(".startTime").append(startTime.toLocaleString(i18n.lng()));
                prot.find(".duration").html(duration.mm + ":" + duration.ss);
                beginIndex++;
            }
        }).fail(function (d) {
            $.notify(i18n.t('notif.loadingRanked') + " -- " + i18n.t('notif.noMore'), "error");
            NProgress.done();stopSpin();
        })
        .done(function (d) {
            $.notify(i18n.t('notif.done') + " -- " + i18n.t('notif.finished'), "success");
            NProgress.done();stopSpin();
            //$(".match.hist .details").click( function() { $("#matchname").html($(this).prop("id")); getMatch($(this).prop("id"), false); });
            $("#historyGroup .expand").click( 
                function(d) { 
                    //$("#m" + $(this).attr("match")).html($(this).attr("match"));
                    getMatch_($("#m" + $(this).attr("match")), $(this).attr("match"));
                }
            );
            /**$("#historyGroup .timeline").click( 
                function(d) { 
                    //$("#m" + $(this).attr("match")).html($(this).attr("match"));
                    getTimeline($(this).attr("match"),$(this));
                }
            );**/
            $("#historyGroup .timeline").attr("disabled", "");
            bootstrap_init();
        });
};

var getTeam = function () {
    $.notify(i18n.t('notif.loadingRanked') + " -- " + i18n.t('notif.pleaseWait'), "info");
    NProgress.start();startSpin();
    $.post(origin + '/history', {id: summonerId, beginIndex: teamIndex, region: region, queues: "TEAM"})
        .success(function (data) {
            var arr = data.arr;
            for (var i = 0; i < arr.length; ++i) {
                var prot = $(".panelProto").clone().removeClass("panelProto");
                var stats = arr[i].participants[0].stats;
                var wl = stats.winner;
                var champ = data.cList[i];
                var spell = data.sList[i];
                var ci = champ.image;
                var di = spell.d.image;
                var fi = spell.f.image;
                var itemCol = data.iList[i];
                var summary = prot.find(".summary");
                var dImg = summary.find(".dImg");
                var fImg = summary.find(".fImg");
                var cImg = summary.find(".cImg");
                dImg.prop("src", spellUrl + di.full);
                fImg.prop("src", spellUrl + fi.full);
                cImg.prop("src", championUrl + ci.full);
                dImg = di ? dImg.popover({trigger:'hover', html : true, title : spell.d.name, content: spell.d.description, template: popoverTempH, placement:'bottom'}) : dImg;
                fImg = fi ? fImg.popover({trigger:'hover', html : true, title : spell.f.name, content: spell.f.description, template: popoverTempH, placement:'bottom'}) : fImg;
                cImg = ci ? cImg.popover({trigger:'hover', html : true, title : champ.name, content: champ.title, template: popoverTempH, placement:'bottom'}) : cImg;                
                prot.find(".panel-collapse").prop("id", "t" + arr[i].matchId);
                summary.find(".expand").prop("href", "#t" + arr[i].matchId).attr("data-parent", "#teamGroup").attr("match", arr[i].matchId);
                summary.find(".timeline").attr("match", arr[i].matchId);
                summary.find(".kills").html(stats.kills + "K");
                summary.find(".deaths").html(stats.deaths + "D");
                summary.find(".queueType").html(i18n.t("gameType." + arr[i].queueType));
                summary.find(".assts").html(stats.assists + "A");
                var ratio = stats.deaths ? Math.round(100 * (stats.kills + stats.assists) / stats.deaths) / 100 : "<code>PERFECT</code>";
                summary.find(".kda").html(ratio);
                summary.find(".lvl").html(i18n.t("global.level") + " " + stats.champLevel);
                for (var m = 0; m < itemCol.length; ++m)
                {
                    var ii = itemCol[m] ? summary.find(".i" + (m + 1)).prop("src", itemUrl + itemCol[m].image.full) : "";
                    ii = (ii == "") ? ii : ii.popover({trigger:'hover',
                                                        html : true, title : itemCol[m].name,
                                                        content: itemCol[m].description, 
                                                        template: popoverTempH, 
                                                        placement: 'bottom'});
                }
                summary.find(".ward").html(stats.sightWardsBoughtInGame);
                summary.find(".vision").html(stats.visionWardsBoughtInGame);
                summary.find(".minions").html(stats.minionsKilled);
                summary.find(".neutral").html(stats.neutralMinionsKilled);
                summary.find(".totalMinions").html((stats.minionsKilled + stats.neutralMinionsKilled));
                summary.find(".golds").html(stats.goldEarned);
                prot.removeClass("hidden");
                prot = wl ? prot.addClass("panel-success") : prot.addClass("panel-danger");
                $("#teamGroup").append(prot);
                var startTime = new Date(arr[i].matchCreation);
                var duration = formatTime(arr[i].matchDuration);
                prot.find(".startTime").append(startTime.toLocaleString(i18n.lng()));
                prot.find(".duration").html(duration.mm + ":" + duration.ss);
                teamIndex++;
            }
        }).fail(function (d) {
            $.notify(i18n.t('notif.loadingRanked') + " -- " + i18n.t('notif.noMore'), "error");
            NProgress.done();stopSpin();
        })
        .done(function (d) {
            $.notify(i18n.t('notif.done') + " -- " + i18n.t('notif.finished'), "success");
            NProgress.done();stopSpin();
            //$(".match.hist .details").click( function() { $("#matchname").html($(this).prop("id")); getMatch($(this).prop("id"), false); });
            $("#teamGroup .expand").click( 
                function(d) { 
                    //$("#m" + $(this).attr("match")).html($(this).attr("match"));
                    getMatch_($("#t" + $(this).attr("match")), $(this).attr("match"));
                }
            );
            /**$("#teamGroup .timeline").click( 
                function(d) { 
                    //$("#m" + $(this).attr("match")).html($(this).attr("match"));
                    getTimeline($(this).attr("match"),$(this));
                }
            );**/
            $("#teamGroup .timeline").attr("disabled", "");
            bootstrap_init();
        });
};
/* GETTING RECENT SECTION */
var recentPlayers = {};
var teamMap = {};
var getRecent = function () {
    $.notify(i18n.t('notif.loadingRecent') + " -- " + i18n.t('notif.pleaseWait'), "info");
    NProgress.start();startSpin();
    $.post(origin + '/recent', {id: summonerId, region: region})
        .success(function (data) {
            var arr = data.arr;
            for (var i = 0; i < arr.length; ++i)
            {
                var p = arr[i];
                teamMap[p.gameId] = p.teamId;
            }
            recentPlayers = data.fMap;
            $("#recentGroup").empty();
            for (var i = 0; i < arr.length; ++i) {
                var stats = arr[i].stats;
                var wl = stats.win;
                var champ = data.cList[i];
                var spell = data.sList[i];
                var prot = $(".panelProto").clone().removeClass("panelProto");
                var ci = champ.image;
                var di = spell.d.image;
                var fi = spell.f.image;
                var itemCol = data.iList[i];
                var summary = prot.find(".summary");
                var dImg = summary.find(".dImg");
                var fImg = summary.find(".fImg");
                var cImg = summary.find(".cImg");
                dImg.prop("src", spellUrl + di.full);
                fImg.prop("src", spellUrl + fi.full);
                cImg.prop("src", championUrl + ci.full);
                dImg = di ? dImg.popover({trigger:'hover', html : true, title : spell.d.name, content: spell.d.description, template: popoverTempH, placement:'bottom'}) : dImg;
                fImg = fi ? fImg.popover({trigger:'hover', html : true, title : spell.f.name, content: spell.f.description, template: popoverTempH, placement:'bottom'}) : fImg;
                cImg = ci ? cImg.popover({trigger:'hover', html : true, title : champ.name, content: champ.title, template: popoverTempH, placement:'bottom'}) : cImg;
                var itemCol = data.iList[i];
                var k = (stats.championsKilled ? stats.championsKilled : 0);
                var d = (stats.numDeaths ? stats.numDeaths : 0);
                var a = (stats.assists ? stats.assists : 0);
                var ratio = stats.numDeaths ?  Math.round(100 * (k + a) / d) / 100 : "PERFECT";
                summary.find(".lvl").html(i18n.t("global.level") + " " + stats.level);
                prot.find(".panel-collapse").prop("id", "r" + arr[i].gameId);
                summary.find(".expand").prop("href", "#r" + arr[i].gameId).attr("data-parent", "#recentGroup").attr("match", arr[i].gameId);
                summary.find(".timeline").attr("match", arr[i].gameId);
                summary.find(".kills").html(k + "K");
                summary.find(".deaths").html(d + "D");
                summary.find(".assts").html(a + "A");
                summary.find(".queueType").html(arr[i].subType == 'NONE' ? i18n.t("gameType." + arr[i].gameType) : i18n.t("gameType." + arr[i].subType));
                summary.find(".kda").html(ratio);
                for (var m = 0; m < itemCol.length; ++m)
                {
                    var ii = itemCol[m] ? summary.find(".i" + (m + 1)).prop("src", itemUrl + itemCol[m].image.full) : "";
                    ii = (ii == "") ? ii : ii.popover({trigger:'hover',
                                                        html : true, title : itemCol[m].name,
                                                        content: itemCol[m].description, 
                                                        template: popoverTempH, 
                                                        placement: 'bottom'});
                }
                var csMin = (stats.minionsKilled ? stats.minionsKilled: 0);
                var csNeu = (stats.neutralMinionsKilled ? stats.neutralMinionsKilled: 0);
                var wards = (stats.sightWardsBought ? stats.sightWardsBought : 0);
                var vision = (stats.visionWardsBought ? stats.visionWardsBought : 0);
                summary.find(".ward").html(wards);
                summary.find(".vision").html(vision);
                summary.find(".minions").html(csMin);
                summary.find(".neutral").html(csNeu);
                summary.find(".totalMinions").html((csMin + csNeu));
                summary.find(".golds").html(stats.goldEarned);
                var startTime = new Date(arr[i].createDate);
                var duration = formatTime(stats.timePlayed);
                prot.find(".startTime").append(startTime.toLocaleString(i18n.lng()));
                prot.find(".duration").html(duration.mm + ":" + duration.ss);
                prot.removeClass("hidden");
                prot = wl ? prot.addClass("panel-success") : prot.addClass("panel-danger");
                $("#recentGroup").append(prot);
            }
        }).fail(function (d) {
            $.notify(i18n.t('notif.loadingRecent') + " -- " + i18n.t('notif.noMore'), "error");
            NProgress.done();stopSpin();
        })
        .done(function (d) {
            $.notify(i18n.t('notif.done') + " -- " + i18n.t('notif.finished'), "success");
            NProgress.done();stopSpin();
            $("#recentGroup .expand").click( 
                function(d) { 
                    //$("#m" + $(this).attr("match")).html($(this).attr("match"));
                    getMatch_($("#r" + $(this).attr("match")), $(this).attr("match"), true);
                }
            );
            /**$("#recentGroup .timeline").click( 
                function(d) { 
                    //$("#m" + $(this).attr("match")).html($(this).attr("match"));
                    getTimeline($(this).attr("match"),$(this));
                }
            );**/
            $("#recentGroup .timeline").attr("disabled", "");
            NProgress.done();stopSpin();
            bootstrap_init();
        });
};
/* GETTING MATCH SECTION */
var getMatch_ = function(section, val, rec)
{
    NProgress.start();
    $.post(origin + "/game", {matchId : val, region: region})
        .success(function(data) {
            startSpin();
            var arr = data.arr;
            var cList = data.cList;
            var iList = data.iList;
            var sList = data.sList;
            var ps = arr.participants;
            var pis = arr.participantIdentities;
            var tList = _.map(arr.participants, function(p) { return p.teamId } );
            if(rec)
            {
                if(section.attr("loaded") == "false")
                {
                    var idArr = _.map(recentPlayers[val], function(p) { return p.summonerId; } );
                    var cArr = _.map(recentPlayers[val], function(p) { return p.championId; } );
                    var team1 = _.filter(recentPlayers[val], function(p) { return p.teamId   == 100; });
                    var team2 = _.filter(recentPlayers[val], function(p) { return p.teamId == 200; });
                    team1 = _.map(team1, function(p) { var res = {}; res["c" + p.championId] = p.summonerId; return res; });
                    team2 = _.map(team2, function(p) { var res = {}; res["c" + p.championId] = p.summonerId; return res; });
                    team1c = _.reduce(team1, _.extend);
                    team2c = _.reduce(team2, _.extend);
                    $.post(origin + "/ids", {id : idArr.length ? idArr.join() : summonerId, region : region})
                        .success(function(data2) {
                            var selfFound = false;
                            var group1 = section.find(".team100").empty();
                            var group2 = section.find(".team200").empty();
                            for(var i = 0; i < ps.length; ++i)
                            {
                                var prot = $(".itemProto").clone().removeClass("itemProto").removeClass("hidden");
                                var cId = ps[i].championId;
                                var sumId = undefined;
                                if (ps[i].teamId == 100)
                                {
                                    sumId = team1c ? team1c["c" + cId] : undefined;
                                }
                                else if (ps[i].teamId == 200)
                                {
                                    sumId = team2c ? team2c["c" + cId] : undefined;
                                }
                                var sumName = undefined;
                                if (sumId)
                                {
                                    sumName = data2[sumId].name
                                } else {
                                    if (ps[i].teamId == teamMap[val] && !selfFound)
                                    {
                                        sumName = summonerName;
                                        selfFound = true;
                                    }
                                    else
                                    {
                                        sumName = "<var>BOT</var>";
                                    }
                                }
                                var stats = ps[i].stats;
                                var wl = stats.win;
                                var champ = cList[i];
                                var spell = sList[i];
                                var ci = champ.image;
                                var di = spell.d ? spell.d.image : "";
                                var fi = spell.f ? spell.f.image : "";
                                var dImg = prot.find(".dImg");
                                var fImg = prot.find(".fImg");
                                var cImg = prot.find(".cImg");
                                dImg.prop("src", spellUrl + di.full);
                                fImg.prop("src", spellUrl + fi.full);
                                cImg.prop("src", championUrl + ci.full);
                                dImg = di ? dImg.popover({trigger:'hover', html : true, title : spell.d.name, content: spell.d.description, template: popoverTempH, placement:'bottom'}) : dImg;
                                fImg = fi ? fImg.popover({trigger:'hover', html : true, title : spell.f.name, content: spell.f.description, template: popoverTempH, placement:'bottom'}) : fImg;
                                cImg = ci ? cImg.popover({trigger:'hover', html : true, title : champ.name, content: champ.title, template: popoverTempH, placement:'bottom'}) : cImg;
                                var itemCol = iList[i];
                                var k = stats.kills;
                                var d = stats.deaths;
                                var a = stats.assists;
                                var kills = prot.find(".kills").html(k + "K");
                                var deaths = prot.find(".deaths").html(d + "D");
                                var assists = prot.find(".assts").html(a + "A");
                                var ratio = d ? Math.round(100 * (k + a) / d) / 100 : "PERFECT";
                                prot.find(".kda").html(ratio);
                                for (var m = 0; m < itemCol.length; ++m)
                                {
                                    var ii = itemCol[m] ? prot.find(".i" + (m + 1)).prop("src", itemUrl + itemCol[m].image.full) : "";
                                    ii = (ii == "") ? ii : ii.popover({trigger:'hover',
                                                                        html : true, title : itemCol[m].name,
                                                                        content: itemCol[m].description, 
                                                                        template: popoverTempH, 
                                                                        placement: 'bottom'});
                                }
                                var csMin = (stats.totalMinionsKilled ? stats.totalMinionsKilled: 0);
                                var csNeu = (stats.neutralMinionsKilled ? stats.neutralMinionsKilled: 0);
                                prot.find(".minions").html(csMin);
                                prot.find(".neutral").html(csNeu);
                                prot.find(".totalMinions").html((csNeu + csMin));
                                prot.find(".sumName").html(sumName);
                                prot.find(".ward").html((stats.sightWardsBoughtInGame ? stats.sightWardsBoughtInGame : 0));
                                prot.find(".vision").html((stats.visionWardsBoughtInGame ? stats.visionWardsBoughtInGame : 0));
                                prot.find(".golds").html((stats.goldEarned));
                                prot.find(".lvl").html(stats.champLevel).css("color","#111111");
                                if (sumName == summonerName)
                                {
                                    prot.addClass("list-group-item-info");
                                }
                                else
                                {
                                    if (wl) {
                                        prot.addClass("list-group-item-success");
                                    } else {
                                        prot.addClass("list-group-item-danger");
                                    }
                                }
                                if (ps[i].teamId == 100)
                                {
                                    group1.append(prot);
                                } else (ps[i].teamId == 200)
                                {
                                    group2.append(prot);
                                }
                            }
                        });
                }
            }
            else {
                if(section.attr("loaded") == "false")
                {
                    var team1 = section.find(".team100").empty();
                    var team2 = section.find(".team200").empty();
                    for(var i = 0; i < pis.length; ++i)
                    {
                        var prot = $(".itemProto").clone().removeClass("itemProto").removeClass("hidden");
                        var sumName = pis[i].player.summonerName;
                        var stats = ps[i].stats;
                        var wl = stats.win;
                        var champ = cList[i];
                        var spell = sList[i];
                        var ci = champ.image;
                        var di = spell.d.image;
                        var fi = spell.f.image;
                        var dImg = prot.find(".dImg");
                        var fImg = prot.find(".fImg");
                        var cImg = prot.find(".cImg");
                        dImg.prop("src", spellUrl + di.full);
                        fImg.prop("src", spellUrl + fi.full);
                        cImg.prop("src", championUrl + ci.full);
                        dImg = di ? dImg.popover({trigger:'hover', html : true, title : spell.d.name, content: spell.d.description, template: popoverTempH, placement:'bottom'}) : dImg;
                        fImg = fi ? fImg.popover({trigger:'hover', html : true, title : spell.f.name, content: spell.f.description, template: popoverTempH, placement:'bottom'}) : fImg;
                        cImg = ci ? cImg.popover({trigger:'hover', html : true, title : champ.name, content: champ.title, template: popoverTempH, placement:'bottom'}) : cImg;
                        var itemCol = iList[i];
                        var k = stats.kills;
                        var d = stats.deaths;
                        var a = stats.assists;
                        var kills = prot.find(".kills").html(k + "K");
                        var deaths = prot.find(".deaths").html(d + "D");
                        var assists = prot.find(".assts").html(a + "A");
                        var ratio = d ? Math.round(100 * (k + a) / d) / 100 : "PERFECT";
                        prot.find(".kda").html(ratio);
                        for (var m = 0; m < itemCol.length; ++m)
                        {
                            var ii = itemCol[m] ? prot.find(".i" + (m + 1)).prop("src", itemUrl + itemCol[m].image.full) : "";
                            ii = (ii == "") ? ii : ii.popover({trigger:'hover',
                                                                html : true, title : itemCol[m].name,
                                                                content: itemCol[m].description, 
                                                                template: popoverTempH, 
                                                                placement: 'bottom'});
                        }
                        var csMin = (stats.totalMinionsKilled ? stats.totalMinionsKilled: 0);
                        var csNeu = (stats.neutralMinionsKilled ? stats.neutralMinionsKilled: 0);
                        prot.find(".minions").html(csMin);
                        prot.find(".neutral").html(csNeu);
                        prot.find(".totalMinions").html((csNeu + csMin));
                        prot.find(".sumName").html(sumName);
                        prot.find(".ward").html((stats.sightWardsBought ? stats.sightWardsBought : 0));
                        prot.find(".vision").html((stats.visionWardsBought ? stats.visionWardsBought : 0));
                        prot.find(".golds").html((stats.goldEarned));
                        prot.find(".lvl").html(stats.champLevel).css("color","#111111");
                        if (sumName == summonerName)
                        {
                            prot.addClass("list-group-item-info");
                        }
                        else
                        {
                            if (wl)
                            {
                                prot.addClass("list-group-item-success");
                            } else
                            {
                                prot.addClass("list-group-item-danger");
                            }
                        }
                        if (ps[i].teamId == 100)
                        {
                            team1.append(prot);
                        } else if (ps[i].teamId == 200)
                        {
                            team2.append(prot);
                        }
                    }
                }
            }
        })
        .fail(function(e) {
            NProgress.done();stopSpin();
        })
        .done(function(d) {
            NProgress.done();stopSpin();section.attr("loaded", "true");
        });
};


/* CONTROLLER SECTION */
$(".popover-content").css("width", "280px");
var compRenew = function()
{
    beginIndex = 0;
    teamIndex = 0;
    getRecent();
    $("#historyGroup").empty();
    $("#teamGroup").empty();
    getHist();
    getTeam();
    //$('html, body').animate({ scrollTop: off }, 'fast');
};

function bootstrap_init()
{
    $(".cs").tooltip({title : i18n.t("global.minions") + " + " + i18n.t("global.monsters") });
}

$("#ver_form").submit(function(event)
{
    $.notify(i18n.t('notif.verifyAttempt') + " -- " + i18n.t('notif.pleaseWait'), "info");
    NProgress.start();startSpin();
    $.post(origin + "/users/verify", {username: $("#ver_username").val(),
                                                        password : $("#ver_pw").val(),
                                                        region: region, id: summonerId,
                                                        status: $("#ver_status").val()})
    .success(function(data) {
        var body = $("#ver_body").empty();
        $("#ver_body").append($("<h2>").html("Welcome! " + summonerName));
    }).fail(function(e) { 
        $.notify(i18n.t('notif.fail') + "\n" + i18n.t('notif.authFail'), "error");
        NProgress.done();stopSpin();
    }).done(function(d) {
        notice.update({
                title: i18n.t('notif.success'),
                text: i18n.t('notif.updateSuccessful'),
                animate_speed: 'fast',
                type: 'success'
            });
            $("#verifyBtn").removeClass("btn-default").addClass("btn-success");
            $("#updateBtn").removeClass("btn-default").addClass("btn-success");
            NProgress.done();stopSpin();
            $('#verify').modal('hide');
            NProgress.start();startSpin();
            window.setTimeout(function() {
                window.location.reload();
            }, 300);
    });
    event.preventDefault();
});

$("#controls").hover(function(){ $(this).css("opacity", "1.0"); }, function(){ $(this).css("opacity", "0.5"); });
$("#ver_username").prop("placeholder", i18n.t("global.username"));
$("#ver_pw").prop("placeholder", i18n.t("global.password"));
$("#goTop").click(function() { $('html, body').animate({ scrollTop: off }, 'fast'); });
$("#getRecentBtn").click(getRecent);
$("#moreHistory").click(getHist);
$("#moreTeam").click(getTeam);

$(document).ready(function()
{
    summary();
    compRenew();
    if(pIcon || summonerName)
    {
        $("#profileIcon")
            .prop("src", iconUrl + pIcon + ".png")
            .css("height", "96px").css("width", "96px");
        $("#currentStatus").removeClass("hidden");
        $.get(origin + '/users/status', {id : summonerId, region : region})
            .success(function(data)
            {
                $("#statusMsg").append($("<blockquote>").append(data));
            })
            .fail(function(e)
            {
                $("#statusMsg").append($("<blockquote>").append("..."));
            })
            .done(function(d)
            {

            });
    }
    $('#historyPane a:first').tab('show');
    $("#fb-btn").append($("<div>").addClass("fb-share-button").attr("data-href", origin + "/fetchPage?region=" + region + "&sumName=" + summonerName).attr("data-layout","box_count"));

});

i18n.init({lng : $.cookie('i18next') ? $.cookie('i18next') : navigator.browserLanguage.substring(0,2) },function(t) {
    $("html").i18n();
    $("#ver_username").prop("placeholder", i18n.t("global.username"));
    $("#ver_pw").prop("placeholder", i18n.t("global.password"));
    $("#ver_status").prop("placeholder", i18n.t("global.message"));
    var appName = t("app.name");
});
