

(function()
{
    var logoDat = 
            {
                "5 6" : "memorial_md.png",
                "5 25" : "memorial_md.png",
                "7 15" : "national_md.png",
                "11 25" : "christmas_mg.png"
            }
    var date = new Date();
    var str = date.getMonth() + " " + date.getDate();
    var filename = logoDat[str] ? logoDat[str] : "logo_.png";
    $("#logo-img").attr("src", "/img/" + filename);
})();

